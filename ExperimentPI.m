% originally Anna J�rgensen 6.2017
%17.10.18 Miriam Faxel --- mixed from Anna and Miri

% Model according to Peng&Chittka
% Absolute and differentiell learning and testing
% functions needed: setupBrain, processInput, modifyWeight, createInput
%%--------------------------------------------------

% Parameters:   please specify
C_Type ='class2'     % class1 or class2 of Kenyon cells (higher connectivity)
train_trials = 5;        % how  many training trials

%for the graphs
PIabsolute=zeros(100,1);
PIdifferential=zeros(100,1);
Winit = ones(1,4000)*0.2; 


%---------------------------
%Create 100 input patterns (stimuli) and their Index for CS+/CS-
[InputPatterns,InputRs,MaxSimilarity,MinSimilarity] = createInput()

% learn absolute learning, by presenting only one stimuli (pattern 40)
PN = InputPatterns(:,40);
R = InputRs(40);

%Set up Brain
[C,W,W_EN_p, W_EN_n,nRows,nCols]=setupBrain(C_Type); % this sets up the connectivity and weights, it is not completely necessary but for the structure 

%learn
for i = 1: train_trials
    [R_KC, R_p,R_n] = processInput(PN, C, W,W_EN_p, W_EN_n); % processes the PN through all the three layers and computes the PI for it
    [W, W_EN_p, W_EN_n] = updateWeight(R, PN, C, W, R_KC, W_EN_p, W_EN_n); % because we are learning, the weights need to be updated according to the CS+/ CS-
    
end

%testing: present model with pattern #40 and all 99 others 
for patterns = 1:100
    PN = InputPatterns(:,patterns);
    [R_KC, R_p,R_n] = processInput(PN, C, W,W_EN_p, W_EN_n);
    PIabsolute(patterns)= -((R_p -R_n)/(Winit*R_KC))*100;
end

% plot PI for all 100  patterns
figure('Name','Preference Indices');
plot(PIabsolute,'-or','color','b');
xlabel('pattern');
ylabel('PI');
set(gca,'Ydir','reverse');
set(gcf,'color','w');

    
%differential training procedure with patterns# 40(CS+) and #54(CS-). 
%10 training rounds each, pseudorandomized 
%testing: present model with pattern #40(+) and #54(-)and all 98 others 

%Set up Brain
[C,W,W_EN_p, W_EN_n,nRows,nCols]=setupBrain(C_Type); % this sets up the connectivity and weights, it is not completely necessary but for the structure 

index = [40 40 40 40 40 40 40 40 40 40 54 54 54 54 54 54 54 54 54 54];
%index = [80 80 80 80 80 80 80 80 80 80  3 3 3 3 3 3 3 3 3 3];
index(randperm(20));

for i = 1:length(index)
    j = index(i)
    PN = InputPatterns(:,j);
    R = InputRs(j);
    [R_KC, R_p,R_n] = processInput(PN, C, W,W_EN_p, W_EN_n); % processes the PN through all the three layers
    [W, W_EN_p, W_EN_n] = updateWeight(R, PN, C, W, R_KC, W_EN_p, W_EN_n);
end

    % testing all 
for patterns = 1:100
    PN = InputPatterns(:,patterns);
    [R_KC, R_p,R_n] = processInput(PN, C, W,W_EN_p, W_EN_n);
    PIdifferential(patterns) = -((R_p-R_n)/(Winit*R_KC))*100;
end  

hold on
plot(PIdifferential,'-or','color','r');
xlabel('pattern');
ylabel('PI');
set(gca,'Ydir','reverse');
legend('PI absolute','PI differential');
set(gcf,'color','w');
    

% calculating PI 
%pi = -((R_p-R_n)/((ones(1,4000)*0.2)*R_KC))*100



