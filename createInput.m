 function [InputPatterns,InputRs,MaxSimilarity,MinSimilarity] = createInput()
%create input patterns and CS

% create a matrix InputPatterns (100,100) that contains 100 columns of
% input patterns (100 neurons each, binary, 50% ones, blockwise)and vector
% of Rs
 

%InputPatterns
 InputPatterns = zeros(100,100); %matrix containing all 100 input patterns
 StartBlock = [1:1:100]; % starting value of the block of ones within a column (pattern)

 
for columns = 1:100
    InputPatterns((StartBlock(columns)):(StartBlock(columns)+49),columns)=1;
    tooLong=0;
    if StartBlock(columns) > 51
        tooLong =(StartBlock(columns)+49)-100;
    end 
   if tooLong > 0
        InputPatterns((1:tooLong),columns)=1;
    end
end 
    
 InputPatterns = InputPatterns(1:length(InputPatterns)-tooLong,:);
 
 %similarity among Patterns calculations
 MaxSimilarityCalc = zeros(100,1);
  
 for index1 = 1:100
  if InputPatterns(index1,1) == 1 && InputPatterns(index1,2) == 1
     MaxSimilarityCalc(index1) = 1;  
  end 
 end
     
MaxSimilarity = sum(MaxSimilarityCalc);

MinSimilarityCalc = zeros(100,1);
  
 for index2 = 1:100
  if InputPatterns(index2,1) == 1 && InputPatterns(index2,51) == 1
     MinSimilarityCalc(index2) = 1;  
  end 
 end
     
MinSimilarity = sum(MinSimilarityCalc);

  
%CS- or CS+
InputRs = zeros(1,100);
rng(48);


 for idx = 1:100
     InputRs(idx) = 2*round(rand)-1;
 end 

