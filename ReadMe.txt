%%%%%  ExperimentPI   %%%%%
Rate Model based on Peng and Chittka 2017

1.) contact: 
- Anna Jürgensen(anna-maria.juergensen@smail.uni-koeln.de),
- Miriam Faxel (mfaxel@smail.uni-koeln.de)

2.) Programs needed: 
- Matlap 2017b https://de.mathworks.com/products/matlab.html

3.) Functions:
- setupBrain(): creates the initial connections- and the weight matrices between the layers.
The class type 1 or 2 defines two different sets of connections 
between PN and KC (Class 1 means one PN contacts 45-55 KCs, class 2 means 5-15)
-createInput(): creates a matrix of 100 stimuli
-processInput():runs the stimuli through the layers, and computes the PI (preference index)
- updateWeight(): if the network should lean, the weights need to be updated depending on CS+/CS-
