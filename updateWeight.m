% this function checks the reward and then updates the weights in both
% Weight matrixes

function [W, W_EN_p, W_EN_n] = updateWeight(R, PN, C, W, R_KC, W_EN_p, W_EN_n)

%updates for the weights
PNKC_p = 0.006;
PNKC_n = 0.007;
KCEN_p = 0.006;
KCEN_n = 0.008;

cnt_pos = 0;
cnt_neg = 0;

% update W
for j= 1: size(W,1) %4000 rows
    for i = 1:size(W,2) % 100 colums
        if R==1 && PN(i)>0 && R_KC(j) ==1 && C(j,i)==1
            W(j,i) = W(j,i)+ PNKC_p;
            if W(j,i) > 0.4
                W(j,i) = 0.4;
            end
            cnt_pos = cnt_pos+1;
        elseif R==-1 && PN(i)>0 && R_KC(j) ==1 && C(j,i)==1
            W(j,i) = W(j,i)- PNKC_n;
            if W(j,i) < 0
              W(j,i) = 0;
            end 
            cnt_neg = cnt_neg+1;
        end 
    end 
    display(j);
end
display(cnt_pos);
display(cnt_neg);

% update W_EN_p and W_EN_n
for i = 1: size(W_EN_p,2)
    if R == 1 && R_KC(i) == 1
        W_EN_p(i) = W_EN_p(i) - KCEN_p;
        if W_EN_p(i) < 0
            W_EN_p(i) = 0;
        end
    end
end

for i = 1: size(W_EN_n,2)
    if R == -1 && R_KC(i)==1
        W_EN_n(i) = W_EN_n(i)- KCEN_n;
        if W_EN_n(i) < 0
            W_EN_n(i) = 0;
        end
    end
end