% function for processing the Input
%[PN]=createPN(brightness);

function [R_KC, R_p,R_n] = processInput(PN, C, W,W_EN_p, W_EN_n)

% genereating KC as a product out of PN and the W (weights)
KC = W*PN;

%inhibitory feedback on KC to generate R_KC
[rubish,idx]=sort(KC);
n=ceil(length(KC)/100*5);
top5pc=idx(end-n+1:end);
R_KC=zeros(size(KC));
R_KC(top5pc)=1;

%generate R_p/ R_n as a product of R_KC and W_EN_p/ W_EN_n
R_p = W_EN_p* R_KC;
R_n = W_EN_n* R_KC;
end 




